# hello-world

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### CD gitlab Page
```
npm run build && xcopy dist public /E /Y && git add . && git commit -m '[feat]update' && git push
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
